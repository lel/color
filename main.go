package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
)

func usage() {
	callName := os.Args[0]
	fmt.Fprintf(os.Stderr, "usage: %s [options] set\n", callName)
	fmt.Fprintf(os.Stderr, "\tMerge sets of Xresources files.\n\n")
	fmt.Fprintf(os.Stderr, "options:\n")
	fmt.Fprintf(os.Stderr, "  -base path\n\t\tuse \"path\" as base directory for Xresources files and set json\n")
	fmt.Fprintf(os.Stderr, "  -list\n\t\tlist sets\n")
	fmt.Fprintf(os.Stderr, "  -show set\n\t\tlist contents of set named\n")
	fmt.Fprintf(os.Stderr, "  -add set file1 file2 ...\n\t\tCreate new set of name \"set\" containing files named.\n")
	fmt.Fprintf(os.Stderr, "  -remove set\n\t\tRemove named set.\n")
	fmt.Fprintf(os.Stderr, "\nOtherwise activates named set(s).\n")
	os.Exit(2)
}

func getBase(base string) (string, error) {
	if base == "" {
		base = path.Join(os.Getenv("HOME"), ".local/share/color")
	}
	if _, err := os.Stat(base); err != nil {
		err := os.MkdirAll(base, os.ModePerm)
		if err != nil {
			return "", err
		}
	}
	return base, nil
}

func main() {
	flagAdd := flag.String("add", "", "add a new set")
	flagRemove := flag.String("remove", "", "remove a set")
	flagList := flag.Bool("list", false, "list sets")
	flagShow := flag.String("show", "", "examine a set")
	flagBase := flag.String("base", "", "change base directory")
	log.SetPrefix(fmt.Sprintf("%s: ", os.Args[0]))
	log.SetFlags(0)
	flag.Usage = usage
	flag.Parse()

	base, err := getBase(*flagBase)
	if err != nil {
		log.Fatal("Couldn't get or make base dir: ", err)
	}

	setFile, err := os.OpenFile(path.Join(base, "sets.json"), os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		log.Fatal("Couldn't make/open json: ", err)
	}

	if *flagAdd != "" {
		if flag.NArg() < 1 {
			flag.Usage()
		}
		files := flag.Args()
		err := add(setFile, *flagAdd, files)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if *flagRemove != "" {
		if flag.NArg() != 0 {
			flag.Usage()
		}
		err := remove(setFile, *flagRemove)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if *flagList {
		err := list(setFile)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if *flagShow != "" {
		if flag.NArg() != 0 {
			flag.Usage()
		}
		err := show(setFile, *flagShow)
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	if flag.NArg() < 1 {
		flag.Usage()
	}

	allSets, err := get(setFile)
	if err != nil {
		log.Fatal(err)
	}

	requestedSetNames := flag.Args()
	requestedSets := [][]string{}
	for _, name := range requestedSetNames {
		if set, ok := allSets[name]; !ok {
			log.Fatal(errors.New("no set by that name"))
		} else {
			requestedSets = append(requestedSets, set)
		}
	}
	err = purge()
	if err != nil {
		log.Fatal(err)
	}
	for i := range requestedSetNames {
		for _, resource := range requestedSets[i] {
			if !filepath.IsAbs(resource) {
				resource = path.Join(base, resource)
			}
			err := merge(resource)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

func get(file *os.File) (map[string][]string, error) {
	byteFile, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var sets map[string][]string
	err = json.Unmarshal(byteFile, &sets)
	if err != nil {
		sets = make(map[string][]string)
	}
	return sets, nil
}

func show(file *os.File, name string) error {
	sets, err := get(file)
	if err != nil {
		return err
	}
	files, ok := sets[name]
	if !ok {
		return errors.New("no such set")
	}
	fmt.Println(files)
	return nil
}

func add(file *os.File, name string, files []string) error {
	sets, err := get(file)
	if err != nil {
		return err
	}
	sets[name] = files
	err = save(file, sets)
	return err
}

func remove(file *os.File, name string) error {
	sets, err := get(file)
	if err != nil {
		return err
	}
	delete(sets, name)
	err = save(file, sets)
	return err
}

func save(file *os.File, sets map[string][]string) error {
	byteFile, err := json.MarshalIndent(sets, "", "\t")
	if err != nil {
		return err
	}
	file.Truncate(0)
	file.Seek(0, 0)
	_, err = file.Write(byteFile)
	return err
}

func list(file *os.File) error {
	sets, err := get(file)
	if err != nil {
		return err
	}
	names := []string{}
	for k := range sets {
		names = append(names, k)
	}
	fmt.Println(names)
	return nil
}

func merge(filePath string) error {
	_, err := os.Stat(filePath)
	if err != nil {
		return err
	}
	cmd := exec.Command("xrdb", "-merge", filePath)
	_, err = cmd.Output()
	return err
}

func purge() error {
	cmd := exec.Command("xrdb", "-remove")
	_, err := cmd.Output()
	return err
}
