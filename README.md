# color

a little tool to manage Xresources

## installation

```bash
go get git.envs.net/lel/color
```

## usage

add a new set of xresources files [contained within base folder, `~/.local/share/color` by default, can change with `-base`]
```bash
$ color -add dark Xresources.blaque Xresources.fira-mono Xresources.base
```

list all of your sets:
```bash
$ color -list
[dark]
```

show the contents of a set:
```bash
$ color -show dark
[Xresources.blaque Xresources.fira-mono Xresources.base]
```

activate a set or sets (basically, this does `xrdb -remove`, then does `xrdb -merge` for every file in the named set or sets):
```bash
$ color dark
```

You can name more than one set when activating a set by just putting more.

delete a set (not the files within!!):
```bash
$ color -remove dark
```

## actual usage (or: how _i_ use it)

This was made so I could easily manage all my Xresources files. I symlink `~/.local/share/color` to my dotfile directory (which i keep in [keybase](https://lel.keybase.pub/share)), then i keep a file called `.colorbase` in `~`, which contains a few sets that I always keep active on this set-up, then I wrote up a little shell script to wrap `color`, that I call recolor:
```bash
#!/usr/bin/env sh

color $(cat $HOME/.colorbase) "$@"
```

As a result, I can define a local set of sets to automatically merge on my current device, but keep all of my dots and `color` sets in `keybase`.

In case you're wondering how I do the dots in `keybase`, i symlink a dotfile dir in my public kbfs TLF to `~/.dots` and then turn on syncing with `keybase fs sync enable /keybase/public/lel`.

If you try to do something similar, be warned that all hell will break loose if keybase doesn't start on boot, so make sure to set that up, and make sure to have a 100% local copy of your most important dots, even if out-of-date, that you can drop in if everything breaks).

The other thing is that you need some sort of system for allowing dots to be somewhat different on different devices. Like, I use the same `.mkshrc` everywhere, but I use a different wm rc file because my devices have different resolutions. The solution I came up with for this is something like this:

```bash
#!/usr/bin/env sh

while IFS= read -r line; do
  trimmed=`echo $line`
  [ -z "$trimmed" ] && continue
  head="$(printf '%s' "$line" | cut -c1)"
  if [ $head = \# ]; then
    continue
  fi
  src=$(echo $line | awk -F " -> " '{print $1}')
  dest=$(echo $line | awk -F " -> " '{print $2}')
  src=$(eval echo $src)
  dest=$(eval echo $dest)
  [ -n "$src" ] && [ -n "$dest" ] && ln -sf $src $dest
done < $HOME/.dot_selections
```

which reads a _local_ file called `$HOME/.dot_selections`, which says something like this:
```
~/.dots/xinit/hidpi -> ~/.xinitrc
~/.dots/spectrwm/big.conf -> ~/.spectrwm.conf
~/.dots/xbindkeys/spectrwm -> ~/.xbindkeysrc
~/.dots/mkshrc -> ~/.mkshrc
```
